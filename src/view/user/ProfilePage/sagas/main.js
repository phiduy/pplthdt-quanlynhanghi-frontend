import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import { takeAction } from "services/forkActionSagas";
import * as infoAPI from "api/user/info";
import { NotificationManager } from "react-notifications";

export function* handleGetUserInfo(action) {
  try {
    let res = yield call(infoAPI.getUserInfo, action.payload);
    yield put(actions.getUserInfoSuccess(res.data.data));
  } catch (err) {
    yield put(actions.getUserInfoFail(err));
  }
}

export function* handlUpdateUserInfo(action) {
  try {
    let res = yield call(infoAPI.updateUserInfo, action.payload);
    if (res.data.success) {
      NotificationManager.success("Cập nhật thành công !", "Thông báo", 2000);
      yield put(actions.updateUserInfoSuccess(res.data.data));
    } else {
      NotificationManager.error("Mật khẩu cũ sai !", "Thông báo", 2000);
      yield put(actions.updateUserInfoFail(res.data.message));
    }
  } catch (err) {
    yield put(actions.updateUserInfoFail(err));
  }
}

/*---------------------------------------------------------------------*/
//Get User Info
export function* getUserInfo() {
  yield takeAction(actions.getUserInfo, handleGetUserInfo);
}
//Update User Info
export function* updateUserInfo() {
  yield takeAction(actions.updateUserInfo, handlUpdateUserInfo);
}
/*---------------------------------------------------------------------*/

export default [getUserInfo, updateUserInfo];
