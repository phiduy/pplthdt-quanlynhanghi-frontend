/**
 * @file constants
 */
export const TOGGLE_FORM = "USER_PROFILE/TOGGLE_FORM";

/*---------------------------------------------------------------------*/

export const GET_USER_INFO = "USER_PROFILE/GET_USER_INFO";
export const GET_USER_INFO_SUCCESS = "USER_PROFILE/GET_USER_INFO_SUCCESS";
export const GET_USER_INFO_FAIL = "USER_PROFILE/GET_USER_INFO_FAIL";

export const UPDATE_USER_INFO = "USER_PROFILE/UPDATE_USER_INFO";
export const UPDATE_USER_INFO_SUCCESS = "USER_PROFILE/UPDATE_USER_INFO_SUCCESS";
export const UPDATE_USER_INFO_FAIL = "USER_PROFILE/UPDATE_USER_INFO_FAIL";
/*---------------------------------------------------------------------*/
export const HANDLE_INPUT_CHANGE = "USER_PROFILE/HANDLE_INPUT_CHANGE";
export const HANDLE_VALIDATE = "USER_PROFILE/HANDLE_VALIDATE";

export const HANDLE_CLEAR = "USER_PROFILE/HANDLE_CLEAR";
