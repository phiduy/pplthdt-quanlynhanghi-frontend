import React, { Component } from "react";
import _ from "lodash";
import { Table } from "reactstrap";

class ProfileShift extends Component {
  render() {
    const { profile } = this.props;
    return (
      <React.Fragment>
        <div className="card-box">
          <div className="d-flex jc-space-between align-item-center">
            <h3>Ca làm việc</h3>
            <p>
              Trạng thái:&nbsp;
              {profile.isInShift ? (
                <span className="badge badge-success">Đang trong ca</span>
              ) : (
                <span className="badge badge-danger">Đang nghỉ</span>
              )}
            </p>
          </div>
          <Table responsive>
            <thead>
              <tr className="text-center">
                <th>Thứ hai</th>
                <th>Thứ ba</th>
                <th>Thứ tư</th>
                <th>Thứ năm</th>
                <th>Thứ sáu</th>
                <th>Thứ bảy</th>
                <th>Chủ nhật</th>
              </tr>
            </thead>
            <tbody>
              <tr className="text-center">
                {_.isEmpty(profile.shifts) ? (
                  <td>Không có ca làm</td>
                ) : (
                  _.get(profile, "shifts", []).map((item, index) => {
                    if (_.isEmpty(item.startTime) && _.isEmpty(item.endTime)) {
                      return <td key={index}>Chưa có thời gian</td>;
                    } else {
                      return (
                        <td key={index}>
                          {item.startTime} - {item.endTime}
                        </td>
                      );
                    }
                  })
                )}
              </tr>
            </tbody>
          </Table>
        </div>
      </React.Fragment>
    );
  }
}

export default ProfileShift;
