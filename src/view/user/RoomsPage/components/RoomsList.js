import React, { Component } from "react";
import { Row } from "reactstrap";
import RoomItem from "./RoomItem";
import LoadingScreen from "../../../../modules/libs/LoadingScreen";
import _ from "lodash";

class RoomsList extends Component {
  render() {
    const { isLoading, rooms } = this.props;
    return (
      <React.Fragment>
        <Row>
          {isLoading ? (
            <LoadingScreen />
          ) : _.isEmpty(rooms) ? (
            <div>Vui lòng thêm phòng mới.</div>
          ) : (
            rooms.map(item => {
              return (
                <RoomItem
                  key={item._id}
                  item={item}
                  actions={this.props.actions}
                />
              );
            })
          )}
        </Row>
      </React.Fragment>
    );
  }
}

export default RoomsList;
