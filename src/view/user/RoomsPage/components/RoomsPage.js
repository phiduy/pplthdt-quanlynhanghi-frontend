import React, { Component } from "react";
import RoomTopBar from "./RoomTopBar";
import RoomList from "./RoomsList";
import CusModal from "./RoomCustomerModal";
import FeeModal from "./RoomFeeModal";
import ServiceModal from "./RoomServiceModal";
import RoomChangeModal from "./RoomChangeModal";
import ShiftChangeModal from "./RoomShifChangeModal";
import { Alert } from "reactstrap";

class RoomsPage extends Component {
  render() {
    const { isNotOwnShift } = this.props;
    if (isNotOwnShift) {
      return (
        <React.Fragment>
          <Alert color="danger">
            Bạn đang không trong ca làm việc, các chức năng sẽ bị vô hiệu hoá!
          </Alert>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <RoomTopBar {...this.props} />
          <RoomList {...this.props} />
          <CusModal {...this.props} />
          <FeeModal {...this.props} />
          <ServiceModal {...this.props} />
          <RoomChangeModal {...this.props} />
          <ShiftChangeModal {...this.props} />
        </React.Fragment>
      );
    }
  }
}

export default RoomsPage;
