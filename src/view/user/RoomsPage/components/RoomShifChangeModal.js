import React, { Component } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  FormFeedback,
  Input
} from "reactstrap";
import moment from "moment";
import _ from "lodash";

class RoomShiftChangeModal extends Component {
  _handleValidate = () => {
    const { isErrorTakeShiftUsername, isErrorTakeShiftPassword } = this.props;
    const { takeShiftUsername, takeShiftPassword } = this.props.data;

    if (_.isEmpty(takeShiftUsername) && _.isEmpty(takeShiftPassword)) {
      this.props.actions.handleValidate({ checkType: "shiftChange" });
      return false;
    } else {
      if (_.isEmpty(takeShiftUsername)) {
        this.props.actions.handleValidate({
          checkType: "shiftChange",
          name: "takeShiftUsername"
        });
        return false;
      } else if (_.isEmpty(takeShiftPassword)) {
        this.props.actions.handleValidate({
          checkType: "shiftChange",
          name: "takeShiftPassword"
        });
        return false;
      } else {
        if (isErrorTakeShiftUsername || isErrorTakeShiftPassword) {
          return false;
        } else {
          return true;
        }
      }
    }
  };

  _handleShiftChange = () => {
    if (this._handleValidate()) {
      const { takeShiftUsername, takeShiftPassword, note } = this.props.data;
      this.props.actions.shiftChange({
        takeShiftUsername,
        takeShiftPassword,
        note,
        timeTranferShift: moment().format()
      });
    }
  };

  _handleClose = () => {
    this.props.actions.toggleModal("shiftChangeModal");
    this.props.actions.handleInputClear("shiftChange");
  };

  render() {
    const {
      toggleShiftChangeModal,
      isProgressing,
      isErrorTakeShiftUsername,
      isErrorTakeShiftPassword
    } = this.props;
    const { takeShiftUsername, takeShiftPassword, note } = this.props.data;
    return (
      <React.Fragment>
        <Modal isOpen={toggleShiftChangeModal}>
          <ModalHeader toggle={this._handleClose}>
            Thông tin giao ca
          </ModalHeader>
          <ModalBody>
            <FormGroup>
              <FormGroup>
                <label>Tên tài khoản</label>
                <Input
                  type="text"
                  name="takeShiftUsername"
                  value={takeShiftUsername}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  invalid={isErrorTakeShiftUsername}
                  maxLength="100"
                  disabled={isProgressing}
                />
                <FormFeedback>Bạn chưa nhập tên tài khoản!</FormFeedback>
              </FormGroup>
              <FormGroup>
                <label>Mật khẩu</label>
                <Input
                  type="password"
                  name="takeShiftPassword"
                  value={takeShiftPassword}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  invalid={isErrorTakeShiftPassword}
                  maxLength="100"
                  disabled={isProgressing}
                />
                <FormFeedback>Bạn chưa nhập mật khẩu !</FormFeedback>
              </FormGroup>
              <FormGroup>
                <label>Ghi chú</label>
                <Input
                  type="textarea"
                  name="note"
                  value={note}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  maxLength="300"
                  disabled={isProgressing}
                />
              </FormGroup>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              className="btn btn-success btn-rounded w-md waves-effect waves-light"
              onClick={this._handleShiftChange}
              disabled={isProgressing}
            >
              {isProgressing ? (
                <div>
                  <div className="spinner-border spinner-custom text-custom">
                    <span className="sr-only">Loading...</span>
                  </div>
                  &nbsp; Đang xử lí...
                </div>
              ) : (
                "Giao ca"
              )}
            </button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

export default RoomShiftChangeModal;
