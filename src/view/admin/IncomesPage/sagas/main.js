import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as incomeAPI from "api/admin/income";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "react-notifications";

export function* handleGetIncomes(action) {
  try {
    let res = yield call(incomeAPI.getIncomes, action.payload);
    if (res.data.success) {
      yield put(actions.getIncomesSuccess(res.data.data));
    } else {
      yield put(actions.getIncomesFail(res.data.message));
    }
  } catch (error) {
    yield put(actions.getIncomesFail(error));
  }
}

export function* handleMoneyDraw(action) {
  try {
    let res = yield call(incomeAPI.moneyDraw, action.payload);
    if (res.data.success) {
      yield put(actions.moneyDrawSuccess(res.data.data));
      NotificationManager.success("Rút tiền thành công !", "Thông báo", 1500);
    } else {
      yield put(actions.moneyDrawFail(res.data.message));
      NotificationManager.error("Rút tiền thất bại !", "lỗi", 2000);
    }
  } catch (error) {
    yield put(actions.moneyDrawFail(error));
  }
}

/*---------------------------------------------------------------------*/
// Get Income
export function* getIncomes() {
  yield takeAction(actions.getIncomes, handleGetIncomes);
}

// Money Draw
export function* moneyDraw() {
  yield takeAction(actions.moneyDraw, handleMoneyDraw);
}
/*---------------------------------------------------------------------*/

export default [getIncomes, moneyDraw];
