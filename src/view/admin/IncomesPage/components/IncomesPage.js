import React, { Component } from "react";
import { Row, Col, FormGroup, Table, FormText } from "reactstrap";
import LoadingScreen from "../../../../modules/libs/LoadingScreen";
import DataTable, { memoize } from "react-data-table-component";
import _ from "lodash";
import moment from "moment";
import DrawModal from "./IncomeDrawMoney";
import "moment/locale/vi"; // without this line it didn't work
moment.locale("vi");

const columns = memoize(props => [
  {
    name: "Phòng",
    selector: "roomName"
  },
  {
    name: "Loại phòng",
    selector: "roomType",
    sortable: true,
    cell: row => {
      if (row.roomType === "cold") {
        return <span className="badge badge-purple">Máy lạnh</span>;
      }
      return <span className="badge badge-primary">Thường</span>;
    }
  },
  {
    name: "Khách hàng",
    selector: "customerName"
  },
  {
    name: "Loại thuê",
    selector: "rentType",
    cell: row => {
      if (row.rentType === "12Hour") {
        return <span className="badge badge-purple">Qua đêm</span>;
      } else {
        return <span className="badge badge-primary">Theo giờ</span>;
      }
    }
  },
  {
    name: "Thời gian thuê",
    selector: "rentHour",
    cell: row => <div>{row.rentHour} tiếng</div>
  },
  {
    name: "Tông tiền",
    selector: "totalPrice",
    cell: row => <div className="text-success">{row.totalPrice} VNĐ</div>
  }
]);

const ExpandedComponent = ({ data, actions }) => (
  <div className="expanded-container">
    <FormGroup>
      <Row style={{ justifyContent: "space-between" }}>
        <div
          style={{
            flex: 3,
            marginRight: "10px"
          }}
        >
          <h3 className="expanded-title text-primary">Phòng {data.roomName}</h3>
          <p>
            <span className="fs-18 txt-bold">Khách hàng:</span>{" "}
            {data.customerName}
          </p>
          <p>
            <span className="fs-18 txt-bold">Chứng minh nhân dân:</span>{" "}
            {data.customerIdentity}
          </p>
          <p>
            <span className="fs-18 txt-bold">Loại thuê:</span>{" "}
            {data.rentType === "12Hour" ? (
              <span className="badge badge-purple">Qua đêm</span>
            ) : (
              <span className="badge badge-primary">Theo giờ</span>
            )}
          </p>
          <p>
            <span className="fs-18 txt-bold">Giờ vào:</span>{" "}
            <span className="text-success">
              {moment(data.checkinDate).format("lll")}
            </span>
          </p>
          <p>
            <span className="fs-18 txt-bold">Giờ ra:</span>{" "}
            <span className="text-danger">
              {moment(data.checkoutDate).format("lll")}
            </span>
          </p>
          <p>
            <span className="fs-18 txt-bold">Thời gian thuê:</span>{" "}
            <span className="text-inverse">{data.rentHour} tiếng</span>
          </p>
        </div>
        <div style={{ flex: 7 }}>
          <h4 className="text-center">Thông tin dịch vụ</h4>
          <Table responsive>
            <thead>
              <tr>
                <th>Dịch vụ</th>
                <th>Số lượng</th>
                <th>Giá tiền</th>
                <th>Tổng tiền</th>
              </tr>
            </thead>
            <tbody>
              {_.isEmpty(_.get(data, "services", [])) ? (
                <tr>
                  <td>Không có dịch vụ nào.</td>
                </tr>
              ) : (
                data.services.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td>{item.serviceName}</td>
                      <td>{item.quantity}</td>
                      <td>{item.price} VNĐ</td>
                      <td className="text-success">
                        {item.price * item.quantity} VNĐ
                      </td>
                    </tr>
                  );
                })
              )}
            </tbody>
          </Table>
        </div>
      </Row>
    </FormGroup>
  </div>
);

class IncomesPage extends Component {
  componentDidUpdate(prevState) {
    const { isMoneyDrawSuccess } = this.props;
    if (
      prevState.isMoneyDrawSuccess !== isMoneyDrawSuccess &&
      isMoneyDrawSuccess
    ) {
      let timeGetIncomes = moment().format();
      this.props.actions.getIncomes({
        timeGetIncomes: moment(timeGetIncomes).add(2, "hours"),
        isDelay: true
      });
    }
  }

  render() {
    const { isGettingIncomes, listIncomes, dashBoard } = this.props;
    return (
      <React.Fragment>
        {isGettingIncomes ? (
          <div className="card-box">
            <LoadingScreen />
          </div>
        ) : (
          <React.Fragment>
            <div className="card-box">
              <Row>
                <Col lg="3" md="3" sm="12">
                  <div className="income-status">
                    <div className="card-box widget-user">
                      <div className="text-center">
                        <h4 className="text-custom" data-plugin="counterup">
                          {dashBoard.totalBillsMoney} VNĐ
                        </h4>
                        <h5>Tông số tiền trong bill hiện tại</h5>
                        <FormText>Tổng số tiền các hoá đơn hiện tại</FormText>
                      </div>
                    </div>
                    <div className="card-box widget-user">
                      <div className="text-center">
                        <h4 className="text-success" data-plugin="counterup">
                          {dashBoard.totalMoney} VNĐ
                        </h4>
                        <h5>Tổng số tiền có thể rút</h5>
                        <FormText>
                          Tổng số tiền hoá đơn hiện tại cộng với số tiền còn lại
                          sau lần rút trước
                        </FormText>
                      </div>
                    </div>
                  </div>
                </Col>
                <Col lg="9" md="9" sm="12">
                  <div className="card-box">
                    <FormGroup>
                      <h3 className="text-center">
                        Thông tin về lần rút tiền trước đó
                      </h3>
                      <Table responsive>
                        <thead>
                          <tr>
                            <th>Tổng số tiền rút lần trước</th>
                            <th>Số tiền rút lần trước</th>
                            <th>Số tiền còn lại sau lần rút trước</th>
                            <th>Thời gian lấy tiền</th>
                            <th>Ghi chú</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td className="text-primary">
                              {dashBoard.totalMoneyLastIncomes} VNĐ
                            </td>
                            <td className="text-danger">
                              {dashBoard.takeMoneyLastIncomes} VNĐ
                            </td>
                            <td className="text-success">
                              {dashBoard.currentMoneyLastIncomes} VNĐ
                            </td>
                            <td>
                              {moment(dashBoard.lastTimeGetIncomes).format(
                                "lll"
                              )}
                            </td>
                            <td>{dashBoard.noteLastIncomes}</td>
                          </tr>
                        </tbody>
                      </Table>
                    </FormGroup>
                  </div>
                  <FormGroup className="text-center">
                    <button
                      type="button"
                      className="btn btn-success btn-rounded w-md waves-effect waves-light"
                      onClick={() =>
                        this.props.actions.toggleModal("moneyDraw")
                      }
                    >
                      Rút tiền
                    </button>
                  </FormGroup>
                </Col>
              </Row>
            </div>

            <div className="card-box">
              <DataTable
                title="Danh sách hoá đơn từ lần lấy tiền trước đó cho đến thời điểm hiện tại"
                data={listIncomes}
                columns={columns(this.props)}
                highlightOnHover
                pagination
                expandableRows
                expandOnRowClicked
                expandableRowsComponent={
                  <ExpandedComponent actions={this.props.actions} />
                }
              />
            </div>
          </React.Fragment>
        )}
        <DrawModal {...this.props} />
      </React.Fragment>
    );
  }
}

export default IncomesPage;
