import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  FormGroup,
  FormFeedback,
  Input,
  Alert
} from "reactstrap";
import _ from "lodash";
import moment from "moment";

class IncomeDrawMoneyModal extends Component {
  _handleDrawMoney = () => {
    const { takeMoney, note } = this.props.data;
    const { isErrorTakeMoney } = this.props;
    if (_.isEmpty(takeMoney)) {
      this.props.actions.handleValidate("takeMoney");
    } else {
      if (!isErrorTakeMoney) {
        this.props.actions.moneyDraw({
          takeMoney,
          note,
          timeGetIncomes: moment().format()
        });
      }
    }
  };
  render() {
    const {
      toggleDraw,
      isErrorTakeMoney,
      isProgressing,
      dashBoard
    } = this.props;

    const { takeMoney, note } = this.props.data;
    return (
      <React.Fragment>
        <Modal
          isOpen={toggleDraw}
          toggle={() => this.props.actions.toggleModal("moneyDraw")}
        >
          <ModalBody>
            <FormGroup>
              <label>Số tiền hiện có</label>
              <Input type="text" value={dashBoard.totalMoney} disabled={true} />
            </FormGroup>
            {Number.parseInt(dashBoard.totalMoney) <= 0 ? (
              <FormGroup>
                <Alert color="danger">Hiện tại không có doanh thu để rút</Alert>
              </FormGroup>
            ) : (
              <React.Fragment>
                <FormGroup>
                  <label>Số tiền cần rút</label>
                  <Input
                    type="text"
                    name="takeMoney"
                    value={takeMoney}
                    onChange={e => this.props.actions.handleInputChange(e)}
                    invalid={isErrorTakeMoney}
                    maxLength="100"
                    disabled={isProgressing}
                  />
                  <FormFeedback>
                    Bạn chưa nhập số tiền cần rút hoặc số tiền lớn số tiền hiện
                    có!
                  </FormFeedback>
                </FormGroup>
                <FormGroup>
                  <label>Ghi chú</label>
                  <Input
                    type="textarea"
                    name="note"
                    value={note}
                    onChange={e => this.props.actions.handleInputChange(e)}
                    maxLength="200"
                    disabled={isProgressing}
                  />
                </FormGroup>
              </React.Fragment>
            )}
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              className="btn btn-success btn-rounded w-md waves-effect waves-light"
              onClick={this._handleDrawMoney}
              disabled={isProgressing || dashBoard.totalMoney <= 0}
            >
              Xác nhận
            </button>
            <button
              type="button"
              className="btn btn-secondary btn-rounded w-md waves-effect waves-light"
              onClick={() => this.props.actions.toggleModal("moneyDraw")}
              disabled={isProgressing}
            >
              Đóng
            </button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

export default IncomeDrawMoneyModal;
