import React, { Component } from "react";
import { Row, Col, Label, FormGroup, FormFeedback, Input } from "reactstrap";
import _ from "lodash";

class SettingsForm extends Component {
  _handleValidate = () => {
    const {
      pricePerHourCommonDayCommonRoom,
      pricePer12HourCommonDayCommonRoom,
      pricePerHourHolidayCommonRoom,
      pricePer12HourHolidayCommonRoom,

      pricePerHourCommonDayColdRoom,
      pricePer12HourCommonDayColdRoom,
      pricePerHourHolidayColdRoom,
      pricePer12HourHolidayColdRoom,

      extraTime
    } = this.props.data;
    const {
      pricePerHourCommonDayCommonRoomError,
      pricePer12HourCommonDayCommonRoomError,
      pricePerHourHolidayCommonRoomError,
      pricePer12HourHolidayCommonRoomError,
      /*--------*/
      pricePerHourCommonDayColdRoomError,
      pricePer12HourCommonDayColdRoomError,
      pricePerHourHolidayColdRoomError,
      pricePer12HourHolidayColdRoomError,
      extraTimeError
    } = this.props;
    if (
      _.isEmpty(pricePerHourCommonDayCommonRoom) &&
      _.isEmpty(pricePer12HourCommonDayCommonRoom) &&
      _.isEmpty(pricePerHourHolidayCommonRoom) &&
      _.isEmpty(pricePer12HourHolidayCommonRoom) &&
      _.isEmpty(pricePerHourCommonDayColdRoom) &&
      _.isEmpty(pricePer12HourCommonDayColdRoom) &&
      _.isEmpty(pricePerHourHolidayColdRoom) &&
      _.isEmpty(pricePer12HourHolidayColdRoom) &&
      _.isEmpty(extraTime)
    ) {
      this.props.actions.handleValidate("all");
      return false;
    } else {
      if (_.isEmpty(pricePerHourCommonDayCommonRoom)) {
        this.props.actions.handleValidate({
          dayType: "normal",
          roomType: "normal",
          price: "perHour"
        });
        return false;
      } else if (_.isEmpty(pricePer12HourCommonDayCommonRoom)) {
        this.props.actions.handleValidate({
          dayType: "normal",
          roomType: "normal",
          price: "per12Hour"
        });
        return false;
      } else if (_.isEmpty(pricePer12HourHolidayCommonRoom)) {
        this.props.actions.handleValidate({
          dayType: "holiday",
          roomType: "normal",
          price: "perHour"
        });
        return false;
      } else if (_.isEmpty(pricePerHourHolidayCommonRoom)) {
        this.props.actions.handleValidate({
          dayType: "holiday",
          roomType: "normal",
          price: "per12Hour"
        });
        return false;
      } else if (_.isEmpty(pricePerHourCommonDayColdRoom)) {
        this.props.actions.handleValidate({
          dayType: "normal",
          roomType: "cold",
          price: "perHour"
        });
        return false;
      } else if (_.isEmpty(pricePer12HourCommonDayColdRoom)) {
        this.props.actions.handleValidate({
          dayType: "normal",
          roomType: "cold",
          price: "per12Hour"
        });
        return false;
      } else if (_.isEmpty(pricePerHourHolidayColdRoom)) {
        this.props.actions.handleValidate({
          dayType: "holiday",
          roomType: "cold",
          price: "perHour"
        });
        return false;
      } else if (_.isEmpty(pricePer12HourHolidayColdRoom)) {
        this.props.actions.handleValidate({
          dayType: "holiday",
          roomType: "cold",
          price: "per12Hour"
        });
        return false;
      } else if (_.isEmpty(extraTime)) {
        this.props.actions.handleValidate("extraTime");
        return false;
      } else {
        if (
          pricePerHourCommonDayCommonRoomError ||
          pricePer12HourCommonDayCommonRoomError ||
          pricePerHourHolidayCommonRoomError ||
          pricePer12HourHolidayCommonRoomError ||
          /*--------*/
          pricePerHourCommonDayColdRoomError ||
          pricePer12HourCommonDayColdRoomError ||
          pricePerHourHolidayColdRoomError ||
          pricePer12HourHolidayColdRoomError ||
          extraTimeError
        ) {
          return false;
        } else {
          return true;
        }
      }
    }
  };

  _handleUpdateSettings = () => {
    if (this._handleValidate()) {
      this.props.actions.updateSettings(this.props.data);
    }
  };

  render() {
    const {
      pricePerHourCommonDayCommonRoom,
      pricePer12HourCommonDayCommonRoom,
      pricePerHourHolidayCommonRoom,
      pricePer12HourHolidayCommonRoom,

      pricePerHourCommonDayColdRoom,
      pricePer12HourCommonDayColdRoom,
      pricePerHourHolidayColdRoom,
      pricePer12HourHolidayColdRoom,
      extraTime
    } = this.props.data;
    const {
      pricePerHourCommonDayCommonRoomError,
      pricePer12HourCommonDayCommonRoomError,
      pricePerHourHolidayCommonRoomError,
      pricePer12HourHolidayCommonRoomError,
      /*--------*/
      pricePerHourCommonDayColdRoomError,
      pricePer12HourCommonDayColdRoomError,
      pricePerHourHolidayColdRoomError,
      pricePer12HourHolidayColdRoomError,
      /*--------*/
      extraTimeError,
      isProgressing
    } = this.props;
    return (
      <React.Fragment>
        <div className="card-box">
          <Row>
            <Col lg="6" md="12" sm="12">
              <h3>Phòng thường</h3>
              <FormGroup>
                <Label>Giá tiền mỗi giờ ngày thường</Label>
                <Input
                  type="number"
                  placeholder="Giá tiền mỗi giờ phòng thường"
                  name="pricePerHourCommonDayCommonRoom"
                  onChange={e => this.props.actions.handleInputChange(e)}
                  value={pricePerHourCommonDayCommonRoom}
                  invalid={pricePerHourCommonDayCommonRoomError}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Dữ liệu không hợp lệ. Giá tiền phải là số
                </FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Giá tiền qua đêm ngày thường</Label>
                <Input
                  type="number"
                  name="pricePer12HourCommonDayCommonRoom"
                  onChange={e => this.props.actions.handleInputChange(e)}
                  value={pricePer12HourCommonDayCommonRoom}
                  invalid={pricePer12HourCommonDayCommonRoomError}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Dữ liệu không hợp lệ. Giá tiền phải là số
                </FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Giá tiền mỗi giờ ngày lễ</Label>
                <Input
                  type="number"
                  name="pricePerHourHolidayCommonRoom"
                  onChange={e => this.props.actions.handleInputChange(e)}
                  value={pricePerHourHolidayCommonRoom}
                  invalid={pricePerHourHolidayCommonRoomError}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Dữ liệu không hợp lệ. Giá tiền phải là số
                </FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Giá tiền qua đêm ngày lễ</Label>
                <Input
                  type="number"
                  name="pricePer12HourHolidayCommonRoom"
                  onChange={e => this.props.actions.handleInputChange(e)}
                  value={pricePer12HourHolidayCommonRoom}
                  invalid={pricePer12HourHolidayCommonRoomError}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Dữ liệu không hợp lệ. Giá tiền phải là số
                </FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>
                  Thời gian trì hoãn dành cho phòng
                </Label>
                <Input
                  type="number"
                  name="extraTime"
                  onChange={e => this.props.actions.handleInputChange(e)}
                  value={extraTime}
                  invalid={extraTimeError}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Dữ liệu không hợp lệ. Giá tiền phải là số
                </FormFeedback>
              </FormGroup>
            </Col>
            <Col lg="6" md="12" sm="12">
              <h3>Phòng máy lạnh</h3>
              <FormGroup>
                <Label>Giá tiền mỗi giờ ngày thường</Label>
                <Input
                  type="number"
                  placeholder="Giá tiền mỗi giờ phòng thường"
                  name="pricePerHourCommonDayColdRoom"
                  onChange={e => this.props.actions.handleInputChange(e)}
                  value={pricePerHourCommonDayColdRoom}
                  invalid={pricePerHourCommonDayColdRoomError}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Dữ liệu không hợp lệ. Giá tiền phải là số
                </FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Giá tiền qua đêm ngày thường</Label>
                <Input
                  type="number"
                  name="pricePer12HourCommonDayColdRoom"
                  onChange={e => this.props.actions.handleInputChange(e)}
                  value={pricePer12HourCommonDayColdRoom}
                  invalid={pricePer12HourCommonDayColdRoomError}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Dữ liệu không hợp lệ. Giá tiền phải là số
                </FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Giá tiền mỗi giờ ngày lễ</Label>
                <Input
                  type="number"
                  name="pricePerHourHolidayColdRoom"
                  onChange={e => this.props.actions.handleInputChange(e)}
                  value={pricePerHourHolidayColdRoom}
                  invalid={pricePerHourHolidayColdRoomError}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Dữ liệu không hợp lệ. Giá tiền phải là số
                </FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Giá tiền qua đêm ngày lễ</Label>
                <Input
                  type="number"
                  name="pricePer12HourHolidayColdRoom"
                  onChange={e => this.props.actions.handleInputChange(e)}
                  value={pricePer12HourHolidayColdRoom}
                  invalid={pricePer12HourHolidayColdRoomError}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Dữ liệu không hợp lệ. Giá tiền phải là số
                </FormFeedback>
              </FormGroup>

              <button
                type="button"
                className="btn btn-primary btn-rounded w-md waves-effect waves-light float-right"
                onClick={this._handleUpdateSettings}
                disabled={isProgressing}
              >
                {isProgressing ? (
                  <div>
                    <div className="spinner-border spinner-custom text-custom">
                      <span className="sr-only">Loading...</span>
                    </div>
                    &nbsp; Đang xử lí...
                  </div>
                ) : (
                  "Cập nhật"
                )}
              </button>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

export default SettingsForm;
