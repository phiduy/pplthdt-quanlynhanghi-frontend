import React, { Component } from "react";
import { Row, Col, Table } from "reactstrap";
import LoadingScreen from "../../../../modules/libs/LoadingScreen";
import _ from "lodash";

class ServiceList extends Component {
  _handleEditService = curService => {
    this.props.actions.handleServiceDetail(curService);
    this.props.actions.toggleForm("openForm");
  };

  render() {
    const { isGettingService, services } = this.props;
    return (
      <React.Fragment>
        <Row>
          {isGettingService ? (
            <LoadingScreen />
          ) : (
            <Col lg="12">
              <div className="card-box">
                {_.isEmpty(services) ? (
                  <div>Vui lòng thêm dịch vụ.</div>
                ) : (
                  <Table responsive>
                    <thead>
                      <tr className="text-center">
                        <th>#</th>
                        <th>Tên dịch vụ</th>
                        <th>Số lượng</th>
                        <th>Giá tiền</th>
                        <th>Công cụ</th>
                      </tr>
                    </thead>
                    <tbody>
                      {services.map((item, index) => {
                        return (
                          <tr key={index} className="text-center">
                            <td>{index + 1}</td>
                            <td>{item.name}</td>
                            <td>{item.quantity}</td>
                            <td>{item.price}</td>
                            <td>
                              <button
                                className="btn btn-icon btn-trans waves-effect waves-light btn-primary m-b-5"
                                onClick={() => this._handleEditService(item)}
                              >
                                <i className="mdi mdi-square-edit-outline" />
                              </button>
                              &nbsp;
                              <button
                                className="btn btn-icon btn-trans waves-effect waves-light btn-danger m-b-5"
                                onClick={() => this.props.actions.toggleModal(item)}
                              >
                                <i className="mdi mdi-trash-can-outline " />
                              </button>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                )}
              </div>
            </Col>
          )}
        </Row>
      </React.Fragment>
    );
  }
}

export default ServiceList;
