import React from "react";
import { Row, Col } from "reactstrap";

const RoomTopBar = props => {
  return (
    <Row>
      <Col sm="4">
        <button
          type="button"
          className="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-20"
          onClick={() => props.actions.toggleForm()}
        >
          {props.toggleForm ? (
            <i className="mdi mdi-window-close"></i>
          ) : (
            <span>
              <i className="mdi mdi-table-plus"></i>&nbsp;Tạo phòng
            </span>
          )}
        </button>
      </Col>
    </Row>
  );
};

export default RoomTopBar;
