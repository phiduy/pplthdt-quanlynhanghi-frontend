import React, { Component } from "react";
import LoadingScreen from "../../../../modules/libs/LoadingScreen";
import DataTable, { memoize } from "react-data-table-component";
import _ from "lodash";
import moment from "moment";

const columns = memoize(props => [
  {
    name: "Phòng",
    selector: "name"
  },
  {
    name: "Loại phòng",
    selector: "type",
    sortable: true,
    cell: row => {
      if (row.type === "cold") {
        return <span className="badge badge-purple">Máy lạnh</span>;
      }
      return <span className="badge badge-primary">Thường</span>;
    }
  },
  {
    name: "Trạng thái",
    selector: "status",
    cell: row => {
      if (row.status === "unused") {
        return <div className="badge badge-warning">Chưa sử dụng</div>;
      } else if (row.status === "used") {
        return <div className="badge badge-danger">Đang sử dụng</div>;
      } else {
        return <div className="badge badge-danger">Đang bảo trì</div>;
      }
    }
  }
]);

const ExpandedComponent = ({ data, actions }) => (
  <div className="expanded-container">
    <div className="expanded-header">
      <h3 className="expanded-title">Thông tin phòng - {data.name}</h3>
      <div className="expanded-tool border-bottom">
        <span
          onClick={() => {
            _.isEmpty(data.customerIdentity) &&
              actions.handleRoomDetail({
                type: "editRoom",
                data
              });
          }}
          className={
            _.isEmpty(data.customerIdentity) ? "" : "cursor-not-allowed"
          }
        >
          <i className="mdi mdi-square-edit-outline text-primary" />
          &nbsp; Chỉnh sửa
        </span>
        <span
          onClick={() => {
            actions.handleRoomDetail({
              type: "delRoom",
              data
            });
            actions.toggleModal("confirmModal");
          }}
          className={
            _.isEmpty(data.customerIdentity) ? "" : "cursor-not-allowed"
          }
        >
          <i className="mdi mdi-trash-can-outline text-danger" />
          &nbsp; Xoá
        </span>
      </div>
    </div>
    {_.isEmpty(data.customerIdentity) ? (
      <p>Phòng đang trống</p>
    ) : (
      <div>
        <p>
          <span className="fs-18 txt-bold">Khách hàng:</span>{" "}
          {data.customerName}
        </p>
        <p>
          <span className="fs-18 txt-bold">Chứng minh nhân dân:</span>{" "}
          {data.customerIdentity}
        </p>
        <p>
          <span className="fs-18 txt-bold">Loại thuê:</span>{" "}
          {data.rentType === "12Hour" ? (
            <span className="badge badge-purple">Qua đêm</span>
          ) : (
            <span className="badge badge-primary">Theo giờ</span>
          )}
        </p>
        <p>
          <span className="fs-18 txt-bold">Giờ vào:</span>{" "}
          <span className="text-success">
            {moment(data.checkinDate).format("lll")}
          </span>
        </p>
      </div>
    )}
  </div>
);

class RoomsList extends Component {
  render() {
    const { isGetRoomsSuccess, rooms } = this.props;
    return (
      <React.Fragment>
        <div className="card-box">
          {isGetRoomsSuccess ? (
            <LoadingScreen />
          ) : _.isEmpty(rooms) ? (
            <div>Vui lòng thêm phòng mới.</div>
          ) : (
            <DataTable
              title="Danh sách phòng"
              data={rooms}
              columns={columns(this.props)}
              highlightOnHover
              pagination
              expandableRows
              expandOnRowClicked
              expandableRowsComponent={
                <ExpandedComponent actions={this.props.actions} />
              }
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default RoomsList;
