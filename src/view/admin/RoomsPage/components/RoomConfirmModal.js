import React from "react";
import { Modal, ModalBody, ModalFooter } from "reactstrap";

const ModalConfirm = props => {
  const { curRoom } = props;
  return (
    <Modal
      centered
      isOpen={props.toggleConfirmModal}
      toggle={() => props.actions.toggleModal("confirmModal")}
    >
      <ModalBody>
        <div className="text-center">
          <i className="mdi mdi-alert-decagram text-danger fs-50" />
          <p>Bạn có chắc chắn muốn xoá phòng này !</p>
        </div>
      </ModalBody>
      <ModalFooter>
        <button
          type="button"
          className="btn btn-danger btn-rounded w-md waves-effect waves-light"
          onClick={() =>
            props.actions.delRoom({
              _id: curRoom._id
            })
          }
        >
          Xoá
        </button>
        <button
          type="button"
          className="btn btn-secondary btn-rounded w-md waves-effect waves-light"
          onClick={() => props.actions.toggleModal("confirmModal")}
        >
          Huỷ
        </button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalConfirm;
