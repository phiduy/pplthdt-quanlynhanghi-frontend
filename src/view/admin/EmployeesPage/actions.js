/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const toggleForm = createAction(CONST.TOGGLE_FORM);
export const toggleModal = createAction(CONST.TOGGLE_MODAL);
/*---------------------------------------------------------------------*/

export const getAllEmployees = createAction(CONST.GET_ALL_EMPLOYEES);
export const getAllEmployeesSuccess = createAction(CONST.GET_ALL_EMPLOYEES_SUCCESS);
export const getAllEmployeesFail = createAction(CONST.GET_ALL_EMPLOYEES_FAIL);

export const getShiftChangeByTime = createAction(CONST.GET_SHIFT_CHANGE_BY_TIME);
export const getShiftChangeByTimeSuccess = createAction(CONST.GET_SHIFT_CHANGE_BY_TIME_SUCCESS);
export const getShiftChangeByTimeFail = createAction(CONST.GET_SHIFT_CHANGE_BY_TIME_FAIL);

export const takeShift = createAction(CONST.TAKE_SHIFT);
export const takeShiftSuccess = createAction(CONST.TAKE_SHIFT_SUCCESS);
export const takeShiftFail = createAction(CONST.TAKE_SHIFT_FAIL);

export const addEmployee = createAction(CONST.ADD_EMPLOYEE);
export const addEmployeeSuccess = createAction(CONST.ADD_EMPLOYEE_SUCCESS);
export const addEmployeeFail = createAction(CONST.ADD_EMPLOYEE_FAIL);

export const updateEmployee = createAction(CONST.UPDATE_EMPLOYEE);
export const updateEmployeeSuccess = createAction(CONST.UPDATE_EMPLOYEE_SUCCESS);
export const updateEmployeeFail = createAction(CONST.UPDATE_EMPLOYEE_FAIL);

export const delEmployee = createAction(CONST.DEL_EMPLOYEE);
export const delEmployeeSuccess = createAction(CONST.DEL_EMPLOYEE_SUCCESS);
export const delEmployeeFail = createAction(CONST.DEL_EMPLOYEE_FAIL);
/*---------------------------------------------------------------------*/
export const handleInputChange = createAction(CONST.HANDLE_INPUT_CHANGE);
export const handleAddCalendar = createAction(CONST.HANDLE_ADD_CALENDAR);
export const handleValidate = createAction(CONST.HANDLE_VALIDATE);
export const handleChangeTab = createAction(CONST.HANDLE_CHANGE_TAB);
export const handleEmployeeDetail = createAction(CONST.HANDLE_EMPLOYEE_DETAIL);
export const handleTimeSelect = createAction(CONST.HANDLE_TIME_SELECT);
export const handleClearField = createAction(CONST.HANDLE_CLEAR_FIELD);
/*---------------------------------------------------------------------*/
export const handleClear = createAction(CONST.HANDLE_CLEAR);
export const handleClearTime = createAction(CONST.HANDLE_CLEAR_TIME);