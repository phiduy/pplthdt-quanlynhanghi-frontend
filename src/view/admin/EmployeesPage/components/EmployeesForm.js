import React, { Component } from "react";
import {
  Row,
  Col,
  Label,
  FormGroup,
  FormFeedback,
  Input,
  Table
} from "reactstrap";
import _ from "lodash";

class EmployeesForm extends Component {
  _handleCreateEmployee = () => {
    const { isUpdate } = this.props;
    if (this._handleValidate()) {
      isUpdate
        ? this.props.actions.updateEmployee(this.props.data)
        : this.props.actions.addEmployee(this.props.data);
    }
  };

  _handleValidateShift() {
    const { shifts } = this.props;
    shifts.map(item => {
      if (item.isErrorShiftStartTime || item.isErrorShiftEndTime) {
        return true;
      } else {
        return false;
      }
    });
  }

  _handleValidate = () => {
    const {
      name,
      identity,
      address,
      salary,
      username,
      password,
      shifts
    } = this.props.data;
    const {
      isErrorName,
      isErrorIdentity,
      isErrorAddress,
      isErrorSalary,
      isErrorUsername,
      isErrorPassword,
      isErrorShifts,
      isUpdate
    } = this.props;
    if (
      _.isEmpty(name) &&
      _.isEmpty(identity) &&
      _.isEmpty(address) &&
      _.isEmpty(salary) &&
      _.isEmpty(username) &&
      _.isEmpty(password)
    ) {
      this.props.actions.handleValidate();
      return false;
    } else {
      if (_.isEmpty(name)) {
        this.props.actions.handleValidate("name");
        return false;
      } else if (_.isEmpty(identity)) {
        this.props.actions.handleValidate("identity");
        return false;
      } else if (_.isEmpty(address)) {
        this.props.actions.handleValidate("address");
        return false;
      } else if (_.isEmpty(salary)) {
        this.props.actions.handleValidate("salary");
        return false;
      } else if (_.isEmpty(username)) {
        this.props.actions.handleValidate("username");
        return false;
      } else if (_.isEmpty(password) && isUpdate === false) {
        this.props.actions.handleValidate("password");
        return false;
      } else if (_.isEmpty(shifts)) {
        this.props.actions.handleValidate("shifts");
        return false;
      } else {
        if (
          isErrorName ||
          isErrorIdentity ||
          isErrorAddress ||
          isErrorSalary ||
          isErrorUsername ||
          isErrorPassword ||
          isErrorShifts || 
          this._handleValidateShift()
        ) {
          return false;
        } else {
          return true;
        }
      }
    }
  };

  render() {
    const {
      isErrorName,
      isErrorIdentity,
      isErrorAddress,
      isErrorSalary,
      isErrorUsername,
      isErrorPassword,
      isErrorShifts,
      isUpdate,
      isProgressing,
      curEmployee
    } = this.props;
    const {
      name,
      identity,
      address,
      salary,
      username,
      password,
      shifts
    } = this.props.data;
    return (
      <React.Fragment>
        <div className="card-box">
          <Row>
            <Col lg="12" md="12" sm="12">
              <FormGroup lg="12" className="text-center">
                <h3>
                  {isUpdate
                    ? `Chỉnh sửa thông tin nhân viên ${curEmployee.name}`
                    : "Thông tin nhân viên"}
                </h3>
                <hr />
              </FormGroup>
            </Col>
            <Col lg="6" md="12" sm="12">
              <FormGroup>
                <Label>Họ tên</Label>
                <Input
                  type="text"
                  name="name"
                  placeholder="Vd: Họ tên nhân viên A"
                  value={name}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  invalid={isErrorName}
                  disabled={isProgressing}
                />
                <FormFeedback>Bạn chưa nhập họ tên nhân viên !</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Chứng minh nhân dân</Label>
                <Input
                  type="text"
                  name="identity"
                  placeholder="Vd: 2511xxxx..."
                  value={identity}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  invalid={isErrorIdentity}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Bạn chưa nhập chứng minh nhân dân hoặc dữ liệu không hợp lệ
                  (Chứng minh nhân dân phải là số và từ 8 kí tự trở lên) !
                </FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Địa chỉ</Label>
                <Input
                  type="text"
                  name="address"
                  placeholder="Vd: Phường 1, quận..."
                  value={address}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  invalid={isErrorAddress}
                  disabled={isProgressing}
                />
                <FormFeedback>Bạn chưa nhập địa chị !</FormFeedback>
              </FormGroup>
            </Col>
            <Col lg="6" md="12" sm="12">
              <FormGroup>
                <Label>Tên tài khoản</Label>
                <Input
                  type="text"
                  name="username"
                  value={username}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  invalid={isErrorUsername}
                  disabled={isProgressing}
                />
                <FormFeedback>Bạn chưa nhập tên tài khoản !</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Mật khẩu</Label>
                <Input
                  type="text"
                  name="password"
                  value={password}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  invalid={isErrorPassword}
                  disabled={isProgressing}
                />
                <FormFeedback>Bạn chưa nhập mật khẩu tài khoản !</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Lương</Label>
                <Input
                  type="text"
                  name="salary"
                  placeholder="Vd: 3000000"
                  value={salary}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  invalid={isErrorSalary}
                  disabled={isProgressing}
                />
                <FormFeedback>
                  Bạn chưa nhập lương hoặc dữ liệu không hợp lệ (Lương của nhân
                  viên phải là số) !
                </FormFeedback>
              </FormGroup>
            </Col>
            <Col lg="12">
              <FormGroup className="text-center">
                <div>
                  <button
                    type="button"
                    className="btn btn-primary btn-trans btn-rounded w-sm waves-effect waves-light m-b-10"
                    onClick={() =>
                      this.props.actions.toggleModal("calendarModal")
                    }
                    disabled={isProgressing}
                  >
                    <i className="mdi mdi-calendar-edit"></i>&nbsp;Thời gian làm việc
                  </button>
                </div>
                <Table responsive>
                  <thead>
                    <tr>
                      <th>Thứ hai</th>
                      <th>Thứ ba</th>
                      <th>Thứ tư</th>
                      <th>Thứ năm</th>
                      <th>Thứ sáu</th>
                      <th>Thứ bảy</th>
                      <th>Chủ nhật</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr className="text-center">
                      {_.isEmpty(shifts) ? (
                        <td colSpan="7">Chưa có lịch làm</td>
                      ) : (
                        shifts.map((item, index) => {
                          if (
                            _.isEmpty(item.startTime) &&
                            _.isEmpty(item.endTime)
                          ) {
                            return <td key={index}>Chưa có thời gian</td>;
                          } else {
                            return (
                              <td key={index}>
                                {item.startTime} - {item.endTime}
                              </td>
                            );
                          }
                        })
                      )}
                    </tr>
                  </tbody>
                </Table>
                <Input type="hidden" invalid={isErrorShifts} />
                <FormFeedback>Chưa có lịch làm cho nhân viên</FormFeedback>
              </FormGroup>
              <FormGroup>
                <div className="float-right m-t-20">
                  <button
                    type="button"
                    className="btn btn-primary btn-rounded w-md waves-effect waves-light"
                    onClick={this._handleCreateEmployee}
                    disabled={isProgressing}
                  >
                    {isProgressing ? (
                      <div>
                        <div className="spinner-border spinner-custom text-custom">
                          <span className="sr-only">Loading...</span>
                        </div>
                        &nbsp; Đang xử lí...
                      </div>
                    ) : (
                      <span>
                        <i className="mdi mdi-arrow-collapse-down" />
                        &nbsp;{isUpdate ? "Lưu thay đổi" : "Thêm mới"}
                      </span>
                    )}
                  </button>
                  &nbsp;
                  {isUpdate && (
                    <button
                      type="button"
                      className="btn btn-secondary btn-rounded w-md waves-effect waves-light"
                      disabled={isProgressing}
                      onClick={() => this.props.actions.handleClearField()}
                    >
                      <i className="mdi mdi-window-close"></i>&nbsp; Huỷ
                    </button>
                  )}
                </div>
              </FormGroup>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

export default EmployeesForm;
