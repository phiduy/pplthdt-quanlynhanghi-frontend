import React, { Component } from "react";
import DataTable, { memoize } from "react-data-table-component";
import { Table } from "reactstrap";
import _ from "lodash";
import LoadingScreen from "../../../../modules/libs/LoadingScreen";

const columns = memoize(props => [
  {
    name: "Họ và tên",
    selector: "name"
  },
  {
    name: "Chứng minh nhân dân",
    selector: "identity",
    sortable: true
  },
  {
    name: "Địa chỉ",
    selector: "address"
  },
  {
    name: "Lương",
    selector: "salary",
    sortable: true,
    cell: row => <div className="text-success">{row.salary} VNĐ</div>
  },
  {
    name: "Tên tài khoản",
    selector: "username"
  },
  {
    name: "Trạng thái",
    selector: "isInShift",
    cell: row => {
      if (row.isInShift) {
        return <div className="badge badge-success">Đang trong ca</div>;
      } else {
        return <div className="badge badge-danger">Đang nghỉ</div>;
      }
    }
  }
]);

const ExpandedComponent = ({ data, actions }) => (
  <div className="expanded-container">
    <div className="expanded-header">
      <h3 className="expanded-title">
        {data.name} - {data.identity}
      </h3>
      <div className="expanded-tool border-bottom">
        <span
          className={data.isInShift ? "cursor-not-allowed" : ""}
          onClick={() => {
            actions.handleEmployeeDetail({
              type: "takeShift",
              data
            });
          }}
        >
          <i className="mdi mdi-briefcase-check text-success" />
          &nbsp; Nhận ca làm việc
        </span>
        <span
          onClick={() => {
            actions.handleEmployeeDetail({
              type: "editEmployee",
              data
            });
          }}
        >
          <i className="mdi mdi-account-edit  text-primary" />
          &nbsp; Chỉnh sửa
        </span>
        <span
          onClick={() => {
            actions.handleEmployeeDetail({
              type: "delEmployee",
              data
            });
            actions.toggleModal("delEmployee");
          }}
        >
          <i className="mdi mdi-trash-can-outline text-danger" />
          &nbsp; Xoá
        </span>
      </div>
    </div>
    <Table responsive>
      <thead>
        <tr>
          <th>Thứ hai</th>
          <th>Thứ ba</th>
          <th>Thứ tư</th>
          <th>Thứ năm</th>
          <th>Thứ sáu</th>
          <th>Thứ bảy</th>
          <th>Chủ nhật</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          {data.shifts.map((item, index) => {
            if (_.isEmpty(item.startTime) && _.isEmpty(item.endTime)) {
              return <td key={index}>Chưa có thời gian</td>;
            } else {
              return (
                <td key={index}>
                  {item.startTime} - {item.endTime}
                </td>
              );
            }
          })}
        </tr>
      </tbody>
    </Table>
  </div>
);

class EmployeesList extends Component {
  componentDidMount() {
    this.props.actions.getAllEmployees();
  }
  render() {
    const { listEmployees, isGettingEmployees } = this.props;
    return (
      <React.Fragment>
        <div className="card-box">
          {isGettingEmployees ? (
            <LoadingScreen />
          ) : (
            <DataTable
              title="Danh sách nhân viên"
              data={listEmployees}
              columns={columns(this.props)}
              pagination
              highlightOnHover
              expandableRows
              expandOnRowClicked
              expandableRowsComponent={
                <ExpandedComponent actions={this.props.actions} />
              }
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default EmployeesList;
