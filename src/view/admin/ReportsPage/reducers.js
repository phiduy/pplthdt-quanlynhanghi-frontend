/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "AdminReportsPage";

const initialState = freeze({
  isGettingReports: false,
  isSelectedMonthReport: false,
  isProgressing: false,
  reports: [],
  data: {},
  monthTotalMoney: 0
});

export default handleActions(
  {
    /*------
    -------- Handle Month Select
    */
    [actions.handleMonthSelect]: (state, action) => {
      return freeze({
        ...state,
        data: {
          ...state.data,
          month: action.payload
        },
        isSelectedMonthReport: true
      });
    },
    /*------
    -------- Get Report Month
    */
    [actions.getReportMonth]: (state, action) => {
      return freeze({
        ...state,
        isGettingReports: true,
        isSelectedMonthReport: false
      });
    },
    [actions.getReportMonthSuccess]: (state, action) => {
      return freeze({
        ...state,
        reports: action.payload.incomes,
        monthTotalMoney: action.payload.monthTotalMoney,
        isGettingReports: false
      });
    },
    [actions.getReportMonthFail]: (state, action) => {
      return freeze({
        ...state,
        isGettingReports: false
      });
    },
    /*------
    -------- Get Report Month
    */
    [actions.downloadReportMonth]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true
      });
    },
    [actions.downloadReportMonthSuccess]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false
      });
    },
    [actions.downloadReportMonthFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false
      });
    }
  },
  initialState
);
