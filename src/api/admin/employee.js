import config from "../../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/admin/employees`;

export function getEmployees(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET");
}

export function takeShift(data) {
  const endpoint = `${baseEndpoint}/${data._id}/takeShift`;
  return request(endpoint, "PUT", data);
}

export function addEmployee(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}

export function updateEmployee(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function delEmployee(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "DELETE", data);
}
