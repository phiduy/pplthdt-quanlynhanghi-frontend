import config from "../../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/admin/reports`;

export function getReportMonth(data) {
  const endpoint = `${baseEndpoint}/month`;
  return request(endpoint, "POST", data);
}

export function downloadReportMonth(data) {
  const endpoint = `${baseEndpoint}/month/download`;
  return request(endpoint, "POST", data);
}
