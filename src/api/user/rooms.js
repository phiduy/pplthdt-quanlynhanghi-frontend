import config from "../../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/users/rooms`;

export function getAllRooms(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET");
}
