import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}`;

export function shiftChange(data) {
  const endpoint = `${baseEndpoint}/users/shifts/tranfer`;
  return request(endpoint, "POST", data);
}

export function getShiftChangeByTime(data) {
  const endpoint = `${baseEndpoint}/admin/shifts/time`;
  return request(endpoint, "POST", data);
}
