import React from "react";
import { Route, Switch } from "react-router-dom";
import ReportsPage from "../../../view/admin/ReportsPage/components/ReportsPageContainer";

const ReportController = ({ match }) => (
  <Switch>
    <Route path={match.url} component={ReportsPage} />
  </Switch>
);
export default ReportController;
