import React from "react";
import {  Route, Switch } from "react-router-dom";
import RoomsPage from "../../../view/admin/RoomsPage/components/RoomsPageContainer";

const EmployeeController = ({ match }) => (
    <Switch>
      <Route path={match.url} component={RoomsPage} />
    </Switch>
);
export default EmployeeController;
