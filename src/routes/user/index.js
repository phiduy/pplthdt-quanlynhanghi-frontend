import React, { Component } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import WrappedMenuContainer from "../../modules/WrappedMenu/components/WrappedMenuContainer";
import Rooms from "./RoomsPage";
import Info from "./ProfilePage";

class App extends Component {
  render() {
    const { match } = this.props;

    return (
      <WrappedMenuContainer>
        <Switch>
          <Redirect exact from={`${match.url}`} to={`${match.url}/info`} />
          <Route path={`${match.url}/info`} component={Info} />
          <Route path={`${match.url}/rooms`} component={Rooms} />
        </Switch>
      </WrappedMenuContainer>
    );
  }
}

export default withRouter(App);
